/*
* hello.c
*
*   @brief: hello world written in C
*   @author: Mark Anthony Cabilo
*   @date: August 2019
*/

#include "stdio.h"

int main(){
    printf("Hello world!\n");
    
    return 0;
}